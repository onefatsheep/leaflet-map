jQuery.noConflict();

(function($) {    
	
		// baseURL
		var baseURL = $("base").attr("href");
		var pageURL = baseURL+$("#MainMap").attr('map-page-url');
		var mapID  = $("#MainMap").attr("map-id");
		
		var MapCategoryHolders;
		var MapCategoryLayers = [];
		
		// create the slippy map
		var map = L.map('MainMap', {
		  minZoom: 1.5,
		  maxZoom: 4,
		  center: [0, 0],
		  zoom: 1.5,
		  crs: L.CRS.Simple,
		});

		get("/api/MapPage/"+mapID,loadBaseMap);
		
		
		// load main map
		function loadBaseMap(json){
			var imageURL = getImage(json.MapImage,4000,2000,"cropped",createBaseMap,json);
		}
		
		function createBaseMap(imageURL,json){
			
			var w = 4000,
			    h = 2000,
			    url = imageURL;

			var southWest = map.unproject([0, h], map.getMaxZoom()-1);
			var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
			var bounds = new L.LatLngBounds(southWest, northEast);

			L.imageOverlay(url, bounds).addTo(map);

			map.setMaxBounds(bounds);
			
			loadCategoryHolders();
		}
		
		// load category holders
		
		function loadCategoryHolders(){
			get("/api/MapCategoryHolder?MapPageID="+mapID,createCategoryHolders);
		}
		
		function createCategoryHolders(json){
			for(var i=0; i<json.length; i++){
				if(json[i].MapCategories){
					for(var c=0; c<json[i].MapCategories.length; c++){
						loadCategories(json[i].MapCategories[c]);
					}
				}
			}
		}
		
		// load categories
		
		function loadCategories(categoryHolderID){
			get("/api/MapCategory?MapCategoryHolderID="+categoryHolderID,createCategories);
		}
		
		function createCategories(json){
			for(var i=0; i<json.length; i++){

				if(json[i].MapCategoryImage){
					var imageURL = getImage(json[i].MapCategoryImage,4000,2000,"cropped",createCategoryImage,json[i]);
				}
				// load poi's
				loadPOIs(json[i].ID);
			}
		}
		
		function createCategoryImage(imageURL,json){

			
			var w = 4000,
			    h = 2000,
			    url = imageURL;
			 
			var southWest = map.unproject([0, h], map.getMaxZoom()-1);
			var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
			var bounds = new L.LatLngBounds(southWest, northEast);

			categoryLayer = L.imageOverlay(url, bounds);
			categoryLayer.CategoryHolderID = json.CategoryHolderID;
			categoryLayer.CategoryID = json.ID;
			
			MapCategoryLayers.push(categoryLayer);
			
			loadPOIs(json.ID);

		}
		
		// load pois
		
		function loadPOIs(categoryID){
			get("/api/MapPOI?MapCategoryID="+categoryID,createPOIs);
		}
		
		function createPOIs(json){
			for(var i=0; i<json.length; i++){
				createPOIMarker(json[i]);
			}
		}
			
		function createPOIMarker(json){

            L.Icon.Default.imagePath = 'leaflet-map/css/images';
            
            var icon = L.divIcon({
            	className: 'map-marker',
            	iconSize:null,
            	html:'<div class="icon" style="background-color: #'+json.Colour+'"></div>'
            });
            
    		var marker = L.marker([51.5, -0.09],{icon: icon}).on('click', onClick).addTo(map);
    		//marker.bindPopup("<b>Hello world!</b><br>I am a popup.");
    		marker.POIID = json.ID;
    		var lat = (json.XPos);
 		    var lng = (json.YPos);
 		    var newLatLng = new L.LatLng(lat, lng);
 		    marker.setLatLng(newLatLng); 
 		    
 		   function onClick(e) {

 			  $("#Form_CreatePOIImageForm_MapPOIID").val(this.POIID);
 			  var markerPos = this.getLatLng();
 			  map.setView(this.getLatLng(), 2);
 			  
 			   // load POI infos
 			  loadPOIImages(this.POIID);
 			  
 			  
 			   
 		   }
 		   
 		  marker.bindPopup("<span style='color: #"+json.Colour+"'>ADD/VIEW IMAGES</span>");
 	        marker.on('mouseover', function (e) {
 	            this.openPopup();
 	        });
 	        marker.on('mouseout', function (e) {
 	            this.closePopup();
 	        });
 		 
			
		}
		
		function loadPOIImages(POIID){
			get("/api/MapPOIImage?MapPOIID="+POIID+"&Approved=1",createPOIImages);
		}
		
		function createPOIImages(json){
			$('#MapPOImages').slickRemoveAll();
			$('#MapPOIBigImages').slickRemoveAll();
			for(var i=0;i<json.length; i++){
				var imageURL = getImage(json[i].Image,100,100,"cropped",createPOIThumbnailImage,json[i]);
				var html = "<div id='MapPOIImageSliderItem_"+json[i].ID+"' class='MapPOIImageSliderItem' poi-image-id='"+json[i].ID+"'></div>";
				$('#MapPOImages').slickAdd(html);
				
				var bigImageURL = getImage(json.Image,800,800,"setheight",createPOIBigImage,json);
				var html = "<div id='MapPOIImageBigItem_"+json[i].ID+"' class='MapPOIImageBigItem' poi-image-id='"+json[i].ID+"'><h3>"+json[i].CategoryTitle+"</h3><h4>"+json[i].Author+"</h4><h4>"+json[i].FormatedDate+"</h4></div>";
				$('#MapPOIBigImages').slickAdd(html);
			}
		}
		
		function createPOIThumbnailImage(imageURL,json){
			//var html = "<div class='MapPOIImageSliderItem' poi-image-id='"+json.ID+"'><img src='"+imageURL+"'/></div>";
			$("#MapPOIImageSliderItem_"+json.ID).html("<img src='"+imageURL+"'/>");
			var bigImageURL = getImage(json.Image,800,800,"setheight",createPOIBigImage,json);
				
			
		}
		
		function createPOIBigImage(imageURL,json){
			$("#MapPOIImageBigItem_"+json.ID).append("<img src='"+imageURL+"'/>");
			//var html = "<div class='MapPOIImageBigItem' poi-image-id='"+json.ID+"'><h3>"+json.CategoryTitle+"</h3><h4>"+json.Author+"</h4><h4>"+json.FormatedDate+"</h4><img src='"+imageURL+"'/></div>";
			//$('#MapPOIBigImages').slickAdd(html);	
		}
		
		/* ajax functions */
		
		/* basic get */
		function get(url,callback){
			
			var url = baseURL+url;
			
			$.getJSON( url )
			  .done(function( json ) {
			    callback(json);
			  })
			  .fail(function( jqxhr, textStatus, error ) {
			    var err = textStatus + ", " + error;
			    console.log( "Request Failed: " + err );
			});

		}
		
		/* get image */
		function getImage(id,width,height,sizetype,callback,json){

			if(id){
				var url = baseURL+"api/ResizedImage/"+id+"?ResizedMethod="+sizetype+"&Width="+width+"&Height="+height;
				$.get( url )
				  .done(function( imageURL ) {
					  callback(imageURL,json);
				  })
				  .fail(function( jqxhr, textStatus, error ) {
				    var err = textStatus + ", " + error;
				    console.log( "Request Failed: " + err );
				});
			}
		}
		
		
		$( document ).ready(function() {
			
			baseURL = $("base").attr("href");
			pageURL = baseURL+$("#MainMap").attr('map-page-url');
			mapID  = $("#MainMap").attr("map-id");
			
			/* POI Image Form */
			$("#Form_CreatePOIImageForm").submit(function(e){
				
				e.preventDefault();
				
				if(!$(".input-attached-file").val()){
					alert("You must upload a photo.");
					return false;
				}
				
				$("#Form_CreatePOIImageForm").hide();
				$("#MapPOILoader").show();
				
				values = {
					   Name: $('#Form_CreatePOIImageForm_Name').val(),
					   Photographer: $('#Form_CreatePOIImageForm_Photographer').val(),
					   AuthorEmail: $('#Form_CreatePOIImageForm_AuthorEmail').val(),
					   MapPOIID: $('#Form_CreatePOIImageForm_MapPOIID').val(),
					   Image: $(".input-attached-file").val()
			   };
				
	  			$.post(pageURL+"/doCreatePOIImageForm",values,function(data){
	  				$("#MapPOISuccess").show().delay(1000).fadeOut(function(){
	  					$('#ImageDropzone').data('dropzoneInterface').clear();
	  					$("#Form_CreatePOIImageForm").fadeIn();
	  				});
					$("#MapPOILoader").hide();
	 			});
				
			});

		   $(".map-cat-changer").click(function(e){
			   var catID = $(this).attr("cat-id");
			   
			   var hex = $(this).find(".menu-circle").css('border-color');

			   for(var i=0; i<MapCategoryLayers.length; i++){
				   
				   if(catID == MapCategoryLayers[i].CategoryID){ 
					   map.removeLayer(MapCategoryLayers[i]);
					   if($(this).hasClass("active")){
						   map.removeLayer(MapCategoryLayers[i]);
						   $(this).removeClass("active");
						   $(this).find(".menu-circle").css({"background-color": "transparent"});
					   }else{
						   map.addLayer(MapCategoryLayers[i]);
						   $(this).find(".menu-circle").css({"background-color": hex});
						   $(this).addClass("active");
					   }
				   }
			   }
			   e.preventDefault();
		   });
		   
		   
		   $('#MapPOImages').slick({
		    	  dots: true,
		    	  speed: 300,
		    	  slidesToShow: 10,
		    	  slidesToScroll: 1,
		    	  asNavFor: '#MapPOIBigImages',
		    	  focusOnSelect: true,
		    	  responsive: [
		    	    {
		    	      breakpoint: 1024,
		    	      settings: {
		    	        slidesToShow: 10,
		    	        slidesToScroll: 1,
		    	        infinite: true,
		    	        dots: true
		    	      }
		    	    },
		    	    {
		    	      breakpoint: 600,
		    	      settings: {
		    	        slidesToShow: 10,
		    	        slidesToScroll: 1
		    	      }
		    	    },
		    	    {
		    	      breakpoint: 480,
		    	      settings: {
		    	        slidesToShow: 5,
		    	        slidesToScroll: 1
		    	      }
		    	    }
		    	  ]
		    });
		   

		   $('#MapPOIBigImages').slick({
		    	  dots: true,
		    	  speed: 300,
		    	  slidesToShow:1,
		    	  slidesToScroll: 1,
		    	  asNavFor: '#MapPOImages'
		    });
		   
		});
		
}(jQuery));