<ul>
<% loop $MapCategoryHolders %>
	<li><h3>$Title</h3>
		<ul>
			<% loop $MapCategories %>
				<li><a class="map-cat-changer" cat-id="$ID" href="#"><span class="menu-circle" style="border-color:#{$Colour}"></span> $Title</a>
			<% end_loop %>
		</ul>
	</li>
<% end_loop %>
</ul>