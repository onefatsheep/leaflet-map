<?php
class MapPOI extends DataObject{

	public static $singular_name = 'Map POI';

	public static $db = array(
		'Title' => 'Varchar(256)',
		'XPos' => 'Varchar(256)',
		'YPos' => 'Varchar(256)',
		'SortOrder' => 'Int'
	);
	
	public static $has_one = array(
		'MapCategory' => 'MapCategory'
	);
	
	public static $has_many = array(
		'MapPOIImages' => 'MapPOIImage'
	);
	
	public static $summary_fields = array(
		'Title'
	);
	
	static $default_sort = "SortOrder ASC";
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$fields->removeByName("SortOrder");
		$fields->removeByName("MapCategoryID");
		$fields->removeByName("MapPOIImages");
		$fields->addFieldToTab("Root.Main", new TextField("Title","Title"));

		$mapPage = MapPage::get()->first();

		$fields->addFieldToTab("Root.Main",new LiteralField("MapImage","<div id='admin-map-image' map-id='".$mapPage->ID."' cat-colour='".$this->MapCategory()->Colour."'></div>"));
		
		if($this->exists()){
			$gridfieldPages = new GridField("MapPOIImages","Map POI Images",$this->MapPOIImages());
			$gridfieldPages->getConfig()
			     ->addComponent(new GridFieldDetailForm())
			     ->addComponent(new GridFieldAddNewButton('toolbar-header-right'))
			     ->addComponent(new GridFieldEditButton())
			     ->addComponent(new GridFieldDeleteAction());
			$fields->addFieldToTab("Root.POIImages", $gridfieldPages);
		}
		
		$fields->addFieldToTab("Root.Main", new TextField("XPos","X Pos"));
		$fields->addFieldToTab("Root.Main", new TextField("YPos","Y Pos"));

		return $fields;
	}
	
	public function onAfterSerialize( &$formattedDataObjectMap ){
  		$formattedDataObjectMap["Colour"] = $this->MapCategory()->Colour;
  	}

}