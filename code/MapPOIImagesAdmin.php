<?php

class MapPOIImagesAdmin extends LeftAndMain implements PermissionProvider {
	private static $url_segment = 'poi-images';
	private static $url_rule = '/$Action';
	private static $menu_title = 'Map POI Images';
	private static $allowed_actions = array(
		'approved',
		'featured',
		'deleteall',
		'deletemarked',
		'hammarked',
		'showtable',
		'spammarked',
		'EditForm',
		'unmoderated'
	);
	
	/**
	 * @return Form
	 */
	public function getEditForm($id = null, $fields = null) {
		if(!$id) $id = $this->currentPageID();
		$form = parent::getEditForm($id);
		$record = $this->getRecord($id);
		if($record && !$record->canView()) {
			return Security::permissionFailure($this);
		}
		$commentsConfig = GridFieldConfig::create()->addComponents(
			new GridFieldFilterHeader(),
			$columns = new GridFieldDataColumns(),
			new GridFieldSortableHeader(),
			new GridFieldPaginator(25),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm(),
			new GridFieldExportButton(),
			new GridFieldEditButton(),
			new GridFieldDetailForm(),
			$manager = new GridFieldBulkManager()
		);
		
		$manager->addBulkAction(
			'approved', 'Approved', 'MapPOIImagesGridFieldBulkAction_Approved', 
			array(
				'isAjax' => true,
				'icon' => 'delete',
				'isDestructive' => true 
			)
		);
		
		$manager->addBulkAction(
			'featured', 'Featured', 'MapPOIImagesGridFieldBulkAction_Approved', 
			array(
				'isAjax' => true,
				'icon' => 'delete',
				'isDestructive' => true 
			)
		);
		
		$manager->addBulkAction(
			'unapproved', 'Un Approve', 'MapPOIImagesGridFieldBulkAction_Approved', 
			array(
				'isAjax' => true,
				'icon' => 'delete',
				'isDestructive' => true 
			)
		);
		
		$manager->addBulkAction(
			'unfeatured', 'Un Feature', 'MapPOIImagesGridFieldBulkAction_Approved', 
			array(
				'isAjax' => true,
				'icon' => 'delete',
				'isDestructive' => true 
			)
		);
		
		$columns->setFieldFormatting(array(
			'ParentTitle' => function($value, &$item) {
				return sprintf(
					'<a href="%s" class="cms-panel-link external-link action" target="_blank">%s</a>',
					Convert::raw2xml($item->Link()),
					Convert::raw2xml($value)
				);
			}
		));
		
		$needs = new GridField(
			'MapPOIImages', 
			'Needs Approval', 
			MapPOIImage::get()->filter('Approved',0),
			$commentsConfig
		);
		
		$moderated = new GridField(
			'MapPOIImagesApproved', 
			_t('MapPOIImagesAdmin.MapPOIImagesApproved'),
			MapPOIImage::get()->filter('Approved',1),
			$commentsConfig
		);
		
		$needsFeatured = new GridField(
			'MapPOIImagesNotFeatured', 
			'Not Featured', 
			MapPOIImage::get()->filter('Featured',0),
			$commentsConfig
		);
		
		$moderatedFeatured = new GridField(
			'MapPOIImagesFeatured', 
			'Is Featured',
			MapPOIImage::get()->filter('Featured',1),
			$commentsConfig
		);
		
		$fields = new FieldList(
			$root = new TabSet(
				'Root',
				new Tab('NeedsApproval', _t('MapPOIImagesAdmin.NeedsApproval', 'Needs Approval'), 
					$needs
				),
				new Tab('MapPOIImage', _t('MapPOIImagesAdmin.Approved', 'Approved'),
					$moderated
				),
				new Tab('NotFeatured', _t('MapPOIImagesAdmin.NotFeatured', 'Not Featured'), 
					$needsFeatured
				),
				new Tab('MapPOIImageFeatured', _t('MapPOIImagesAdmin.IsFeatured', 'Is Featured'),
					$moderatedFeatured
				)
			)
		);
		
		$root->setTemplate('CMSTabSet');
		$actions = new FieldList();
		
		$form = new Form(
			$this,
			'EditForm',
			$fields,
			$actions
		);
		$form->addExtraClass('cms-edit-form');
		$form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
		if($form->Fields()->hasTabset()) { 
			$form->Fields()->findOrMakeTab('Root')->setTemplate('CMSTabSet');
			$form->addExtraClass('center ss-tabset cms-tabset ' . $this->BaseCSSClasses());
		}
		$this->extend('updateEditForm', $form);
		return $form;
	}
}