<?php
class MapCategoryHolder extends DataObject{

	public static $singular_name = 'Map Category Holder';

	public static $db = array(
		'Title' => 'Varchar(256)',
		'SortOrder' => 'Int'
	);
	
	public static $has_one = array(
		'MapPage' => 'MapPage'
	);
	
	public static $has_many = array(
		'MapCategories' => 'MapCategory'
	);
	
	public static $summary_fields = array(
		'Title'
	);
	
	static $default_sort = "SortOrder ASC";
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$fields->removeByName("SortOrder");
		$fields->removeByName("MapPageID");
		$fields->addFieldToTab("Root.Main", new TextField("Title","Title"));
		
		if($this->exists()){
			$gridfieldPages = new GridField("MapCategories","Map Categories",$this->MapCategories());
			$gridfieldPages->getConfig()
			     ->addComponent(new GridFieldDetailForm())
			     ->addComponent(new GridFieldAddNewButton('toolbar-header-right'))
			     ->addComponent(new GridFieldEditButton())
			     ->addComponent(new GridFieldDeleteAction())
			     ->addComponent(new GridFieldSortableRows('SortOrder'));
			$fields->addFieldToTab("Root.Main", $gridfieldPages);
		}
		return $fields;
	}

}