<?php
class MapPage extends Page {

	public static $db = array(
		
	);
	
	public static $has_one = array(
		"MapImage" => "Image"
	);
	
	public static $has_many = array(
		"MapCategoryHolders" => "MapCategoryHolder"
	);

	public static $icon = 'map/images/map';

	private static $allowed_children = "none";

	function getCMSFields() {
		
		Requirements::css("leaflet-map/css/map_admin.css");
		Requirements::javascript("leaflet-map/javascript/jquery.pep.js");
		Requirements::css("leaflet-map/css/leaflet.css");
		Requirements::javascriptTemplate("leaflet-map/javascript/leaflet.js", $this);
		Requirements::javascript("leaflet-map/javascript/map_admin.js");
		
		$fields = parent::getCMSFields();
		$fields->removeByName("Content");
		$fields->addFieldToTab("Root.Main", $uploadfield = new UploadField("MapImage","Map Image"));
		$uploadfield->setFolderName("MapPage");
		
		$gridfieldPages = new GridField("MapCategoryHolders","Map Category Holders",$this->MapCategoryHolders());
		$gridfieldPages->getConfig()
		     ->addComponent(new GridFieldDetailForm())
		     ->addComponent(new GridFieldAddNewButton('toolbar-header-right'))
		     ->addComponent(new GridFieldEditButton())
		     ->addComponent(new GridFieldDeleteAction())
		     ->addComponent(new GridFieldSortableRows('SortOrder'));
		$fields->addFieldToTab("Root.Main", $gridfieldPages);
		
		return $fields;
	}

}

class MapPage_Controller extends Page_Controller {

	private static $allowed_actions = array (
		'CreatePOIImageForm',
		'doCreatePOIImageForm'
	);

	public function init() {
		parent::init();
		//Requirements::themedCSS('slick');
		Requirements::css("leaflet-map/css/map.css");
		Requirements::css("leaflet-map/css/leaflet.css");
		Requirements::css("leaflet-map/css/slick.css");
		Requirements::javascript("leaflet-map/javascript/jquery-2.1.1.min.js");
		Requirements::javascriptTemplate("leaflet-map/javascript/leaflet.js", $this);
		Requirements::javascriptTemplate("leaflet-map/javascript/slick.min.js", $this);
		Requirements::javascriptTemplate("leaflet-map/javascript/map.js", $this);
	}
	
	public function CreatePOIImageForm(){

	    $fields = new FieldList(
	    	new TextField('Name', "Rider's Name"),
	    	new TextField('Photographer', "Photographer's Name"),
	    	new TextField('AuthorEmail', 'Your Email'),
	    	$file = FileAttachmentField::create('Image', 'Upload Your Photo'),
	    	new TextField("MapPOIID")
		);
		
		$file->setView('grid')->imagesOnly()->setFolderName("MapPOIImages")->setMaxFilesize(5);

	    // Create action
	    $actions = new FieldList(
			new FormAction('doCreatePOIImageForm', 'Submit')
	    );
		// Create action
		$validator = new RequiredFields('Name','Email');
			
		$Form = new Form($this, 'CreatePOIImageForm', $fields, $actions, $validator);	
		
	 	return $Form;
	}
	
	public function doCreatePOIImageForm($data){
		
		$POIImage = new MapPOIImage();
		$POIImage->ImageID = $data["Image"];	
		$POIImage->Author = $data["Name"];
		$POIImage->Photographer = $data["Photographer"];
		$POIImage->AuthorEmail = $data["AuthorEmail"];
		$POIImage->Date = SS_Datetime::now()->Rfc2822();
		$POIImage->MapPOIID = $data["MapPOIID"];
		$POIImage->write();
		
		Debug::dump($data);
		
	}

}
