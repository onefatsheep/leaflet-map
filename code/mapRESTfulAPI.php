<?php
class mapRESTfulAPI extends RESTfulAPI
{
	private static $allowed_actions = array(
		'ResizedImage'
	);

  	function index(SS_HTTPRequest $request)
  	{

  		if($request->param("ClassName") == "ResizedImage"){
  			return $this->ResizedImage($request);
  		}
  		
	    //check authentication if enabled
	    if ( $this->authenticator )
	    {
	    	
	      $policy     = $this->config()->authentication_policy;
	      $authALL    = $policy === true;
	      $authMethod = is_array($policy) && in_array($request->httpMethod(), $policy);

	      if ( $authALL || $authMethod )
	      {
	        $authResult = $this->authenticator->authenticate($request);
	
	        if ( $authResult instanceof RESTfulAPI_Error )
	        {
	          //Authentication failed return error to client
	          return $this->error($authResult);
	        }
	      }
	    }
	
	    //pass control to query handler
	    $data = $this->queryHandler->handleQuery( $request );
	    //catch + return errors
	    if ( $data instanceof RESTfulAPI_Error )
	    {
	      return $this->error($data);
	    }
	
	    //serialize response
	    $json = $this->serializer->serialize( $data );
	    //catch + return errors
	    if ( $json instanceof RESTfulAPI_Error )
	    {
	      return $this->error($json);
	    }
	
	    //all is good reply normally
	    return $this->answer($json);
  	}
	
  	/*
  	 * avalible resize values
  	 * cropped, padded, setwidth, setheight, ratio
  	 * setwidth will be used by default
  	 */
	public function ResizedImage(SS_HTTPRequest $request){
		
		if( $image = Image::get()->filter("ID",$request->param("ID"))->first() ){
			$type = $request->getVar("ResizedMethod");
			
			switch ($type) {
			    case "cropped":
			    	if(!$request->getVar("Width") || !$request->getVar("Height")){
			    		return "You must set a Width and Height"; 
			    	}
			        $resizedImage = $image->CroppedImage($request->getVar("Width"),$request->getVar("Height"))->AbsoluteURL;
			        return $resizedImage;
			        break;
			    case "padded":
					if(!$request->getVar("Width") || !$request->getVar("Height")){
			    		return "You must set a Width and Height"; 
			    	}
			        $resizedImage = $image->PaddedImage($request->getVar("Width"),$request->getVar("Height"))->AbsoluteURL;
			        return $resizedImage;
			        break;
			    case "setheight":
					if(!$request->getVar("Height")){
			    		return "You must set a Height"; 
			    	}
			        $resizedImage = $image->SetHeight($request->getVar("Height"))->AbsoluteURL;
			        return $resizedImage;
			        break;
			    case "ratio":
			    	if(!$request->getVar("Width") || !$request->getVar("Height")){
			    		return "You must set a Width and Height"; 
			    	}
			        $resizedImage = $image->SetRatioSize($request->getVar("Width"),$request->getVar("Height"))->AbsoluteURL;
			        return $resizedImage;
			        break;
			    case "setwidth":
			    	if(!$request->getVar("Width")){
			    		return "You must set a Width"; 
			    	}
			    	$resizedImage = $image->SetWidth($request->getVar("Width"))->AbsoluteURL;
			        return $resizedImage;
			}
		}else{
			return "Sorry this image does not exist";
		}
		
	}
		
}