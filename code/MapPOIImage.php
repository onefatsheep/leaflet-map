<?php
class MapPOIImage extends DataObject{

	public static $singular_name = 'Map POI';

	public static $db = array(
		'Title' => 'Varchar(256)',
		'Approved' => 'Boolean',
		'Author' => 'Varchar(256)',
		'Photographer' => 'Varchar(256)',
		'AuthorEmail' => 'Varchar(256)',
		'Featured' => 'Boolean',
		'Date' => 'SS_DateTime'
	);
	
	public static $has_one = array(
		'Image' => 'Image',
		'MapPOI' => 'MapPOI'
	);
	
	public static $summary_fields = array(
		'Thumbnail',
		'Author',
		'AuthorEmail'
	);
	
	static $default_sort = "Date DESC";
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();

		//$fields->removeByName("MapPOIID");
		
		$fields->addFieldToTab("Root.Main", new TextField("Title","Title"));
		
		$fields->addFieldToTab("Root.Main",$datef =  new DatetimeField("Date","Date"));
		$datef->getDateField()->setConfig('showcalendar', true);
		
		$fields->addFieldToTab("Root.Main",new CheckboxField("Approved","Approved"));
		$fields->addFieldToTab("Root.Main",new CheckboxField("Featured","Featured"));
		
		$fields->addFieldToTab("Root.Main", $uploadfield = new UploadField("Image","Image"));
		$uploadfield->setFolderName("MapPOIImages");

		return $fields;
	}
	
	function getThumbnail(){
	  	if ($this->Image()->exists()){
	  		//return $this->Logo()->CMSThumbnail();
	  		return $this->Image()->getFormattedImage('CroppedImage', 100, 100);
	  	}else{
	  		return '(No Image)';
	  	}
	}
	  
	public function onAfterSerialize( &$formattedDataObjectMap ){
  		$formattedDataObjectMap["CategoryTitle"] = $this->MapPOI()->MapCategory()->Title;
  		$date = new Date();
  		$date->setValue($this->Date);
  		$formattedDataObjectMap["FormatedDate"] = $date->Format('d M Y');
  	}
  	
  	public function getCategory(){
  		return $this->MapPOI()->MapCategory();
  	}

}